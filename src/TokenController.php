<?php

namespace ALL\Auth;

use Illuminate\Support\Str;
use DateTime;
use ALL\Auth\Token;

class TokenController
{
    public static string $extend_expiration_by = "+3 month";    //extended time of token expiration

    /**
     * Create a A-LL Token for the user and device.
     * return the newly created token.
     */
    public function create($user_id, $device)
    {
        $token = Token::updateOrCreate(
            [
                'user_id' => $user_id, 
                'device' => $device
            ],
            [
                'bearer_token' => Str::random(60),
                'expires_at' => (new DateTime(TokenController::$extend_expiration_by))->format('c')
            ]
        );

        return $token;
    }

    /**
     * Check token validity
     * return true if token is valid, false otherwise.
     */
    public function check($bearer_token)
    {
        $token = Token::where('bearer_token', $bearer_token)->first();

        if(! $token || $token->expires_at < (new DateTime())->format('c'))
        {
            //If token exists, but is expired, revoke all expired tokens
            if($token){
                $this->revoke();
            }
            return false;
        }
        
        $token->expires_at = (new DateTime(TokenController::$extend_expiration_by))->format('c');
        $token->save();

        return true;
    }

    /**
     * revoke invalid tokens
     */
    public function revoke()
    {
        $tokens = Token::where('expires_at', '<', (new DateTime())->format('c'))->delete();
    }


}
