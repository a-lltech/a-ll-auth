<?php

namespace ALL\Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $guarded = [];

    protected $hidden = ['user_id', 'device'];
    
}
